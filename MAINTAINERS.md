# When preparing a release of Erasmus:
  - ```cd Math```
  - ```git pull```
# Math/SomeUtils is submoduled from lhcb/Math in this way:
  - ```git submodule add ssh://git@gitlab.cern.ch:7999/lhcb/Math.git```

# Old instructions (to be updated):
  - Instructions for maintainers
  - Carlos Vazquez Sierra (Mar, 2017)
    + Fetch Phys/B2XTauNuTools from vrenaudi/B2XTauNuTools. # Ask Victor Renaudin before.

