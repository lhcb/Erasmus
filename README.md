# The ERASMUS project

# **The Erasmus project has been retired from CVMFS and is not maintained anymore**

Erasmus is just a collection of packages used by various working groups. The structure is rather free. It depends on [Bender](https://gitlab.cern.ch/lhcb/Bender) (and thus in [DaVinci](https://gitlab.cern.ch/lhcb/DaVinci)).

If you are not interested in those dependencies, and rather prefer to use latest heptools, please consider using [Urania](https://gitlab.cern.ch/lhcb/Urania) instead.

## How to work with Erasmus

For development purposes of any of the packages hosted under Erasmus, please follow the [instructions](https://twiki.cern.ch/twiki/bin/view/LHCb/Git4LHCb#Satellite_projects) at the Git4LHCb twiki.

In any case, there are two important rules you must remember:

1. <b>Never clone the whole project for development purposes.</b> Please follow the instructions above.
2. <b>Always checkout the packages from the master branch.</b> If not, when asking for a Merge Request (MR), incompatibilities will appear and thus the MR will be closed. 

Only project maintainers are allowed to accept and revert MR.

### Use this project from the nightlies

```
lb-run -c best --nightly-cvmfs --nightly lhcb-head Erasmus/HEAD bash
```

## Mailing list

For issues, questions and problems; please send an e-mail to:

[<b>lhcb-erasmus-developers@cern.ch</b>](mailto:lhcb-erasmus-developers@cern.ch)

## About 

The Erasmus Bridge (Dutch: Erasmusbrug) is a combined cable-stayed and bascule bridge in the centre of Rotterdam, connecting the north and south parts of this city, second largest in the Netherlands. The bridge was named after Desiderius Erasmus a.k.a. Erasmus of Rotterdam, a prominent Christian renaissance humanist [(+ info)](https://en.wikipedia.org/wiki/Erasmusbrug).




